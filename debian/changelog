emacs-wgrep (3.0.0+20.g208b9d0-2) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.5.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 25 Jul 2024 19:58:57 +0900

emacs-wgrep (3.0.0+20.g208b9d0-1) unstable; urgency=medium

  [ Xiyue Deng ]
  * Keep wgrep-test-helper.el in autopkgtest to fix missing file issue.
  * Add d/upstream/metadata.

  [ Xiyue Deng and Nicholas D Steeves ]
  * Import additional upstream work on wgrep-test and wgrep-test-helper, and
    squash it into 0002-fix-test-extra-loading.patch.

  [ Nicholas D Steeves ]
  * Import a snapshot of upstream at commit:208b9d0, because it appears that
    upstream intended for b36bf59 (three commits prior) to be 3.0.1.  Of the
    work since then, one commit is a bugfix for tests, one is Emacs 28
    linting, and one is irrelevant to the Debian package.
  * Rebase quilt series onto this snapshot.
  * Drop 0002-fix-test-extra-loading.patch.
  * Add 0002-Skip-wgrep-deadgrep-normal.patch.

 -- Nicholas D Steeves <sten@debian.org>  Tue, 23 Jan 2024 14:20:40 -0500

emacs-wgrep (3.0.0-2) unstable; urgency=medium

  * Team upload.
  * Add patch to move common test utility to a separate module to avoid
    double loading existing ert-deftests. (Closes: #1057559)
  * Add Upstream-Contact to d/copyright.

 -- Xiyue Deng <manphiz@gmail.com>  Wed, 06 Dec 2023 17:09:52 -0800

emacs-wgrep (3.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Update my email address.
  * Drop version qualifier from the Recommends for emacs, because even oldstable
    has a new enough version.
  * Update upstream copyright years.
  * Declare Standards-Version 4.6.2 (no changes required).

 -- Nicholas D Steeves <sten@debian.org>  Tue, 14 Feb 2023 18:53:31 -0500

emacs-wgrep (2.3.2+9.gf0ef9bf-2) unstable; urgency=medium

  * Mention helm-ag in elpa-wgrep-helm's long description.
  * Do source-only upload to unblock migration to testing.

 -- Nicholas D Steeves <nsteeves@gmail.com>  Fri, 24 Jul 2020 19:44:26 -0400

emacs-wgrep (2.3.2+9.gf0ef9bf-1) unstable; urgency=medium

  * Initial release. (Closes: #944986)

 -- Nicholas D Steeves <nsteeves@gmail.com>  Wed, 10 Jun 2020 21:10:28 -0400
